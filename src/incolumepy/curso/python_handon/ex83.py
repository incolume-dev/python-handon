# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'


# Write a Python program to sort (ascending and descending) a dictionary by value.
# Original dictionary :  {0: 0, 1: 2, 2: 1, 3: 4, 4: 3}
# Dictionary in ascending order by value :  [(0, 0), (1, 2), (2, 1), (3, 4), (4, 3)]
# Dictionary in descending order by value :  [(4, 3), (3, 4), (2, 1), (1, 2), (0, 0)]


def sort(d: dict, ascending=True):
    result = [(k, v) for k, v in d.items()]
    if ascending:
        result.sort(key=lambda x: x[1])
    else:
        result.sort(key=lambda x: x[1], reverse=True)
    return result


if __name__ == '__main__':
    print(sort({0: 0, 1: 2, 2: 1, 3: 4, 4: 3}))
    print(sort({0: 0, 1: 2, 2: 1, 3: 4, 4: 3}, False))
