# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'
# Write a Python program to generate a series of unique random numbers
import random


def random_num(count):
    nums = set()
    for i in range(count):
        while True:
            num = random.randint(0, 10 ** 2)
            if not num in nums:
                nums.add(num)
                break
    return sorted(nums)


def random_num1(count):
    return sorted(random.sample(range(10**2), count))


if __name__ == '__main__':
    print(random_num1(8))
