# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'

    # Write a Python program to remove leading zeros from an IP address.
    # Input
    # "216.08.094.196"
    # Output
    # 216.8.94.196
import re


def removezero(entrance):
    return re.sub('0', '', entrance)


if __name__ == '__main__':
    print(removezero("216.080.094.196"))
