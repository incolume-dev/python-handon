import toml
import pathlib

__root__ = pathlib.Path(__file__).parent.parent.parent.parent.parent
assert __root__.is_dir(), f'{__root__=}'
__version__ = toml.load(__root__/'pyproject.toml')['tool']['poetry']['version']


if __name__ == '__main__':
    print(__version__)
