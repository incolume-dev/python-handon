# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'


# Write a Python program to flip a coin 1000 times and count heads and tails.
from random import choice


def flipacoin():
    options = {'cara': 0, 'coroa': 0}
    coin = list(options.keys())
    for i in range(1000):
        options[choice(coin)] += 1
    return options


if __name__ == '__main__':
    print(flipacoin())
