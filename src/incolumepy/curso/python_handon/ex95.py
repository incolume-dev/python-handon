# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'

# Write a Python program to read a random line from a file.
# Using test.txt
from random import choices


def randomline(filename):
    with open(filename) as f:
        return choices(f.readlines())


if __name__ == '__main__':
    print(randomline('test.txt'))
