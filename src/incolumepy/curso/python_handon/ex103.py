# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'


# Write a Python program to convert a binary number to decimal number.
def binary2decimal(num: str) -> int:
    return int(num, 2)


if __name__ == '__main__':
    print(binary2decimal('1010'))
    print(binary2decimal('11111111'))
