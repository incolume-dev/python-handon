# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'


# Write a Python program for sequential search. Sequential Search: In computer science, linear search or sequential
# search is a method for finding a particular value in a list that checks each element in sequence until the desired
# element is found or the list is exhausted. The list need not be ordered.
def Sequential_Search(dlist, item):
    pos = 0
    found = False

    while pos < len(dlist) and not found:
        if dlist[pos] == item:
            found = True
        else:
            pos = pos + 1

    return found, pos


def sequential_search(entrance, item):
    for i, elem in enumerate(entrance):
        if elem == item:
            return elem, i


if __name__ == '__main__':
    print(Sequential_Search([11, 23, 58, 31, 56, 77, 43, 12, 65, 19], 31))
    print(sequential_search([11, 23, 58, 31, 56, 77, 43, 12, 65, 19], 31))
