# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'
import datetime as dt


# Write a Python program to convert Year/Month/Day to Day of Year in Python
def convert(date):
    d = dt.datetime.strptime(date, '%Y/%m/%d')
    print(d.timetuple().tm_yday)
    print(d.strftime('%j'))


if __name__ == '__main__':
    convert('1978/6/20')
    convert('2021/1/8')
