# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'


def fatorial(x):
    if x <= 1:
        return 1
    else:
        return x * fatorial(x - 1)


if __name__ == '__main__':
    print(fatorial(5))
