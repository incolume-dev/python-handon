# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'

# Write a Python program to get the file size of a plain file.
# Use test.txt file at same folder
import pathlib


def filesize(filename):
    return pathlib.Path(filename).stat().st_size


if __name__ == '__main__':
    print(filesize('test.txt'))
