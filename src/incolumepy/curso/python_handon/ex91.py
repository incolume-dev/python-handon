# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'

# Write a Python program to read first n lines of a file.
# Use test.txt file


def firstlines(filename, numlines):
    with open(filename) as f:
        return f.readlines()[:numlines]


if __name__ == '__main__':
    print(firstlines('test.txt', 1))
    print(firstlines('test.txt', 3))



