# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'

# Write a Python program to get current time in milliseconds in Python
import datetime as dt

# print(dt.datetime.now().strftime('%s'))
print(dt.datetime.now().timestamp()*1000)
