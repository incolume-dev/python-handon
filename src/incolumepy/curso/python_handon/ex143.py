# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'
# Remove all whitespaces from a string
#
# Input
# ' Python    Exercises '
# Output
# PythonExercises
import re


def removespaces(entrance):
    return re.sub(r'\s+', '', entrance, re.I)


if __name__ == '__main__':
    print(removespaces(' Python    Exercises '))
