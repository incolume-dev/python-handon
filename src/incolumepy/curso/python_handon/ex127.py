# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'
# Write a Python script to display the various Date Time formats.
# a) Current date and time
# b) Current year
# c) Month of year
# d) Week number of the year
# e) Weekday of the week
# f) Day of year
# g) Day of the month
# h) Day of week
import datetime as dt


def infodate():
    return f"""
    a) {dt.datetime.now()}
    b) {dt.datetime.now().strftime('%Y')}
    c) {dt.datetime.now().strftime('%B')}
    d) {dt.datetime.now().strftime('%W')}
    e) {dt.datetime.now().weekday()} {dt.datetime.now().strftime('%w')}
    f) {dt.datetime.now().strftime('%j')}
    g) {dt.datetime.now().strftime('%d')}
    h) {dt.datetime.now().strftime('%A')}
    """


if __name__ == '__main__':
    print(infodate())
