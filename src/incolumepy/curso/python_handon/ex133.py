# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'

# all word contain 5 chracters
# Write a Python program to find all five characters long word in a string.
# Input
# 'The quick brown fox jumps over the lazy dog.'
# Output
# ['quick', 'brown', 'jumps']
import re


def word5chars(entrance):
    return re.findall(r'\w{5}', entrance, re.I)


if __name__ == '__main__':
    print(word5chars('The quick brown fox jumps over the lazy dog.'))
