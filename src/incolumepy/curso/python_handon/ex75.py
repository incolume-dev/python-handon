# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'
# word count
# Write a Python program to count the occurrences of each word in a given sentence
import string
import re


def word_count(s: str) -> dict:
    d = {}

    for word in re.sub('[,.!?:;]', '', s).lower().split():
        if word in d:
            d[word] += 1
        else:
            d[word] = 1
    return d


if __name__ == '__main__':
    s = 'Jesus cristo é o senhor, Jesus Cristo é o Senhor, é?!'
    print(word_count(s))
    print(string.punctuation)
