# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'


def missingelement(container1, container2) -> list:
    return set(container1).difference(container2)


if __name__ == '__main__':
    print(missingelement(range(8), [3, 7, 6, 2, 0, 1, 4]))
