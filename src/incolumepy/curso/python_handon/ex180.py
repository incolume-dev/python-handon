# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'


def fibonacci(n):
    if 0 < n <= 2:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


if __name__ == '__main__':
    print(fibonacci(10))
