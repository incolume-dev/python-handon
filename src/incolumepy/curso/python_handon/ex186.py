# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'


def sumrecursivelist(l: list) -> int:
    try:
        return l.pop() + sumrecursivelist(l)
    except IndexError:
        return 0


if __name__ == '__main__':
    print(sumrecursivelist([x for x in range(10)]))
