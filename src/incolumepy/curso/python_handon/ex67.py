# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'

# remove nth character
# Write a Python program to remove the nth index character from a nonempty string


def remove_nth_char(s: str, n: int) -> str:
    if not s:
        raise ValueError('s must nonempty')
    return s[:n] + s[n+1:]


if __name__ == '__main__':
    s = 'abcdefgh'
    print(remove_nth_char(s, 4))
