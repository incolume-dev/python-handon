# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'
import datetime as dt


# Write a Python program to subtract five days from current date
def subtract_days():
    return dt.datetime.today() - dt.timedelta(days=5)


if __name__ == '__main__':
    print(subtract_days())
