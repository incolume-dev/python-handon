## Sobre este curso

URL: https://www.udemy.com/course/python-handon/learn/lecture/8019052#overview


Most Complete Collection of Python Exerciser and Solution. Cover from Fundamental to Algorithm and Data Analysis

Pelos números

Nível de experiência: Todos os níveis

Alunos: 50898

idiomas: Portuguese

Legendas: Não

Lições: 200

Vídeo: 1,5 horas no total

Recursos

Disponível em iOS e Android

## Descrição

Have 3 secret words to master coding in any language are : practice, practice and practice !!!. Coding is a skill, you will be better when you do it more and more.

This course is the most comprehensive collection of python exercise and solution. It contains more than 200 coding problem and will provide you

great environment for practicing Python programming language.


The exercises cover following topic :

    Fundamental

        basic

        condition and loop

        list

        string

        dictionary

        file

        class

        math

        date time

        regex

    Algorithm

        sort

        search

        recursion

    Data Analysis

        numpy

        pandas



After complete this course, sure you will much more confident with you Python skill and job interview.

Take this course, start boot up your Python coding skill !!!.
O que você aprenderá

    Solve assignments in many areas including data analysis, algorithms, etc ...
    Knowing your level of Python skill.
    Be handed more than 200 solution of all more than 200 assignments
    Make your basic skill strong with all Fundamental assignments
    Be very confident about your solving problem skill at end of course

Há algum requisito ou pré-requisito para o curso?

    Student should have a basic knowledge with Python

Para quem é este curso:

    Do you want to boot up Python skill to next level
    Do you want a complete practical environment for Python
    Do you want to dig down more with important topic : algorithm, data analysis

Instrutor
Foto do usuário
Tan Pham

Deep Learning Engineer

As said by Andrew Ng "AI is the new electricity", in very near future AI will take a very important role in our world.

so, That is reason why I do and teach deep learning. My principles of teaching :

- Start from simple

- Move on step by step

- Explain concept, idea in simple, clear way with visualization

- Practice with code

- Build and apply to real life projects
